/**
 * Created by User on 22/12/2017.
 */

public class BadCardOperation {

    String deletion;
    String timeStamp;
    BadCard badCard;

    public String getDeletion() {
        return deletion;
    }

    public void setDeletion(String deletion) {
        this.deletion = deletion;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public BadCard getBadCard() {
        return badCard;
    }

    public void setBadCard(BadCard badCard) {
        this.badCard = badCard;
    }
}
