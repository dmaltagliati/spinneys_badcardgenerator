/**
 * Created by User on 22/12/2017.
 */
public class BadCardsGenerator {

    public static void main (String[] args){

        System.out.println("INIZIO !!!");
        Configuration.getInstance().loadMainParameters();
        Generator genera = new Generator();
        int numeroFile;
        try {
            numeroFile = Integer.parseInt(args[0]);
            System.out.println("Numero file: " + numeroFile);
        }catch (Exception e){
            numeroFile = 1;
        }

        for (int i=0; i<numeroFile; i++) {
            System.out.println("GENERO FILE N. " + i);
            try {
                Thread.sleep(1000);
            }catch (Exception e){

            }

            genera.go(i);
            genera = new Generator();
        }
        System.out.println("FINITO !!!");
    }
}
