import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Created by User on 22/12/2017.
 */
public class Configuration {

    private static Configuration instance;

    public static Configuration getInstance(){
        if (instance == null)
            instance = new Configuration();

        return instance;
    }

    public  String outputPath = "";
    public  void loadMainParameters(){
        Properties paramProperties = new Properties();
        try {
            paramProperties.load(new FileInputStream("conf" + File.separatorChar + "badCardsGenerator.properties"));
            outputPath = paramProperties.getProperty("outputPath");

        }catch (Exception e){
            System.out.println("eccezione e: " + e.getMessage());
        }

    }

}
