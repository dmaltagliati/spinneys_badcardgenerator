import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by User on 22/12/2017.
 */
public class Generator {

    Gson myGson = new Gson();
    BadCardOperation bCardop = new BadCardOperation();
    BadCard badCard = new BadCard();
    String myBadCard;

    Date sec = new Date();

    Random r1 = new Random(sec.getTime());


    long card = r1.nextInt(900000000);
    int numeroDicarte = r1.nextInt(500);
    SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDhhmmss");

    ArrayList<BadCardOperation> bCardpList = new ArrayList<BadCardOperation>();

    public void go(int numFile) {
        String cardCode = "";
        int deletion = 0;
        System.out.println("GENERO " + numeroDicarte + " carte");
        for (int i = 0; i < numeroDicarte; i++) {
            card = r1.nextInt(900000000);
            cardCode = String.format("%010d", card);
            badCard = new BadCard(cardCode);

            deletion = r1.nextInt(10);
            bCardop = new BadCardOperation();
            bCardop.setBadCard(badCard);
            bCardop.setDeletion(deletion%2 == 0 ? "X" : " ");
            bCardop.setTimeStamp(dateFormat.format(new Date()));

            bCardpList.add(bCardop);
        }
        System.out.println("CARTE GENERATE");
        String fileName = "BadCards_" + dateFormat.format(new Date()) + "_" + String.format("%03d", numFile) + ".json";
        myBadCard = myGson.toJson(bCardpList );
        //System.out.println(myBadCard);
        try {
            System.out.println("Scrivo file : " + fileName);
            FileOutputStream badCardsFile = new FileOutputStream(Configuration.getInstance().outputPath + File.separatorChar + fileName);
            PrintStream daScrivere = new PrintStream(badCardsFile);
            daScrivere.print(myBadCard);
        }catch (Exception e){
            System.out.println("failed to write file. e: "+ e.getMessage());
        }
    }
}
