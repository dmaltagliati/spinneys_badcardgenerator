

public class BadCard {
    private int id;
    private String cardCode;

    public BadCard() {}

    public BadCard(String cardCode) {
        this.cardCode = cardCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    @Override
    public String toString() {
        return "BadCard{" +
                "id=" + id +
                ", cardCode='" + cardCode + '\'' +
                '}';
    }
}
